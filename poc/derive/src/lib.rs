use proc_macro::TokenStream;

use syn::DeriveInput;
use syn::parse_macro_input;
use quote::quote;

#[proc_macro_derive(AsPoint, attributes(point))]
pub fn derive_as_point(item: TokenStream) -> TokenStream {
  let item = parse_macro_input!(item as DeriveInput);
  use syn::{Data, Fields};
  match &item.data {
    Data::Struct(st) => {
      let st_id = &item.ident;
      let point_fields = match &st.fields {
        Fields::Named(nf) => nf,
        _ => unimplemented!()
      }.named.iter().filter(|f| {
        f.attrs.iter().any(|attr| {
          attr.path.is_ident("point")
        })
      }).collect::<Vec<_>>();
      if point_fields.len() != 1 {
        quote! {
          compile_error!("Expected one field with \"point\" attribute.");
        }
      } else {
        let field_name = point_fields[0].ident.as_ref().unwrap();
        quote! {
          impl crate::kdtree_ds::AsPoint for #st_id {
            fn as_point(&self) -> (f32, f32) {
              crate::kdtree_ds::AsPoint::as_point(&self.#field_name)
            }
          }
        }
      }
    },
    Data::Enum(_) => {
      unimplemented!()
    },
    Data::Union(_) => {
      unimplemented!()
    }
  }.into()
}
