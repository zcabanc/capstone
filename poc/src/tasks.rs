use std::ops::Range;

use crate::*;
use crate::AsPoint;
use std::collections::BTreeSet;
use std::mem::replace;
use rand::{thread_rng, Rng};
use std::cmp::Ordering;
use chrono::{Timelike, Datelike};

pub fn add_council(server: &mut Server, name: String, email: String, password: ScryptData, admin_region: RegionOfSpace) -> Ptr<LocalCouncil> {
  let council = LocalCouncil {
    administrative_region: admin_region,
    name,
  };
  let council = to_ptr(council);
  let cred = LoginCredential {
    email: email.clone(),
    password,
    authorisation: CredentialAuthorisation::LocalCouncil(council.clone()),
  };
  server.credentials.insert(email, to_ptr(cred));
  council
}

pub fn add_vulnerable(server: &mut Server, council_auth: Ptr<LoginCredential>, vulnerable: Vulnerable, email: String, password: ScryptData) -> Result<Ptr<Vulnerable>, &'static str> {
  let council = council_auth.unwrap_council()?;
  let vulnerable_point = vulnerable.home_address.as_point();
  if !council.administrative_region.hit_test(vulnerable_point) {
    return Err("Unauthorized point.");
  }
  let vulnerable = to_ptr(vulnerable);
  let cred = LoginCredential {
    email: email.clone(),
    password,
    authorisation: CredentialAuthorisation::Vulnerable(vulnerable.clone()),
  };
  server.credentials.insert(email, to_ptr(cred));
  Ok(vulnerable)
}

pub fn add_volunteer(server: &mut Server, name: String, home_address: Address, phone_number: String, email: String, password: ScryptData) -> Ptr<Volunteer> {
  let volunteer = to_ptr(Volunteer {
    lock: Mutex::new(()),
    name,
    home_address,
    phone_number,
    planned_trips: DisjointIntervals::new(),
    work_time: DisjointIntervals::new(),
    reserved_time: DisjointIntervals::new(),
    delivery_area: RegionOfSpace::empty(),
    max_weight_grams: 5000,
  });
  let cred = LoginCredential {
    email: email.clone(),
    password,
    authorisation: CredentialAuthorisation::Volunteer(volunteer.clone()),
  };
  server.credentials.insert(email, to_ptr(cred));
  server.volunteers_kd.insert(volunteer.clone());
  volunteer
}

pub fn add_payment_method(server: &mut Server, auth: Ptr<LoginCredential>, payment_card: PaymentCard, _cvc: String) -> Result<(), &'static str> {
  let mut vul = auth.unwrap_vulnerable()?;
  // todo: verify card with bank
  vul.payment_methods.push(payment_card);
  Ok(())
}

pub fn register_store(server: &mut Server, name: String, owner_email: String, owner_password: ScryptData, address: Address, opening_time: [Range<NaiveTime>; 7]) -> Ptr<Store> {
  let store = Store {
    name,
    address,
    inventory: BTreeMap::new(),
    opening_time,
  };
  let store = to_ptr(store);
  let cred = LoginCredential {
    email: owner_email.clone(),
    password: owner_password,
    authorisation: CredentialAuthorisation::StoreAdmin(store.clone()),
  };
  server.credentials.insert(owner_email, to_ptr(cred));
  server.stores_kd.insert(store.clone());
  store
}

pub fn add_inventory(_server: &mut Server, store_auth: Ptr<LoginCredential>, item: Item) -> Result<(), &'static str> {
  let mut store = store_auth.unwrap_store()?;
  (*store.inventory.entry(item.name.clone()).or_insert(Item {
    name: item.name.clone(),
    amount_left: Mutex::new(0),
    weight_grams: item.weight_grams,
  }).amount_left.lock().unwrap()) += *item.amount_left.lock().unwrap();
  Ok(())
}

pub fn volunteer_set_worktime(server: &mut Server, volunteer_auth: Ptr<LoginCredential>, chunks: Vec<Range<NaiveDateTime>>) -> Result<(), &'static str> {
  let mut volunteer = volunteer_auth.unwrap_volunteer()?;
  let volunteer = &mut *volunteer;
  let lock = volunteer.lock.lock().unwrap();
  let now = server.time_now();
  volunteer.work_time = DisjointIntervals::try_from_iter(chunks.into_iter().filter(|c| {
    if c.start >= c.end {
      return false;
    }
    if c.end < now {
      return false;
    }
    true
  }))?;
  let existing_reserved_time = replace(&mut volunteer.reserved_time, DisjointIntervals::new());
  let new_reserved_time = &mut volunteer.reserved_time;
  let mut days_to_add = BTreeSet::new();
  for wt in volunteer.work_time.iter_all() {
    days_to_add.insert(wt.start.date());
  }
  for day in days_to_add.into_iter() {
    if let Some(existing_reserved) = existing_reserved_time.iter_intersected(day_time_range(day)).next() {
      if volunteer.work_time.intersects_interval(existing_reserved.clone()) {
        new_reserved_time.insert(existing_reserved.clone()).unwrap();
        continue;
      }
    }

    let planned_orders = &volunteer.planned_trips;
    let mut available_1h_chunks_that_day: Vec<Range<NaiveDateTime>> = volunteer.work_time.iter_intersected(day_time_range(day))
        .filter(|x| x.start > day.and_time(NaiveTime::from_hms(0, 0, 0)))
        .filter(|x| {
          x.end - x.start >= Duration::hours(1)
        })
        .flat_map(|x| {
          let mut v = Vec::new();
          let mut p = x.start;
          while p + Duration::hours(1) <= x.end {
            v.push(p..(p + Duration::hours(1)));
            p += Duration::hours(1);
          }
          v
        })
        .filter(|x| !planned_orders.intersects_interval((*x).clone()))
        .collect();
    if available_1h_chunks_that_day.is_empty() {
      continue;
    }
    let chosen_idx = thread_rng().gen_range(0usize, available_1h_chunks_that_day.len());
    let _ = new_reserved_time.insert(available_1h_chunks_that_day.swap_remove(chosen_idx));
  }
  drop(lock);
  Ok(())
}

pub fn vulnerable_search_nearest_stores(server: &mut Server, vulnerable_auth: Ptr<LoginCredential>, limit: usize) -> Result<Vec<Ptr<Store>>, &'static str> {
  let vulnerable = vulnerable_auth.unwrap_vulnerable()?;
  Ok(server.stores_kd.nearest_iter(vulnerable.as_point()).take(limit.min(20)).cloned().collect())
}

/// # Algorithm overview
///
/// 1. Create a min priority queue of volunteers, sorted by the first date that is possible for this volunteer
/// to accept this order, and then by distance.
///
/// 2. For all volunteers nearby, place them into the priority queue.
///
/// 3. For each volunteer,
pub fn place_order(server: &mut Server, vulnerable_auth: Ptr<LoginCredential>, store_ptr: Ptr<Store>, items: &[(String, u32)], is_urgent: bool) -> Result<Ptr<PlannedTrip>, &'static str> {
  if items.is_empty() {
    return Err("Can't place empty order");
  }
  let vulnerable = vulnerable_auth.unwrap_vulnerable()?;
  if vulnerable.payment_methods.is_empty() {
    return Err("Expected at least one payment method.");
  }
  let store = &mut *store_ptr.clone();
  let store_inv = &mut store.inventory;
  for (it, cnt) in items.iter() {
    if let Some(v) = store_inv.get(it) {
      if *v.amount_left.lock().unwrap() < *cnt {
        return Err("Not enough items left.");
      }
    } else {
      return Err("Can't find item.");
    }
  }
  let mut total_weight = 0u32;
  let mut rollback_log: Vec<(String, u32)> = Vec::with_capacity(items.len());
  fn rollback(log: &[(String, u32)], inv: &mut BTreeMap<String, Item>) {
    for it in log {
      let amount_left = &inv.get(&it.0).unwrap().amount_left;
      *amount_left.lock().unwrap() += it.1;
    }
  }
  for (it, cnt) in items.iter() {
    let item = store_inv.get(it).unwrap();
    let mut amount = item.amount_left.lock().unwrap();
    if *amount < *cnt {
      drop(amount);
      rollback(&rollback_log, store_inv);
      return Err("Not enough items left.");
    } else {
      *amount -= *cnt;
      total_weight += item.weight_grams * *cnt;
      rollback_log.push((it.clone(), *cnt));
    }
  }
  let vulnerable_point = vulnerable.as_point();
  #[derive(Debug, Clone)]
  struct VolunteerToConsider {
    pub time_taken: Duration,
    pub try_allocate_slot: CandidateSlot,
    pub volunteer: Ptr<Volunteer>,
  }
  #[derive(Debug, Clone)]
  enum CandidateSlot {
    NewSlot { slot_time: Range<NaiveDateTime>, starting_point: (f32, f32) },
    Reuse(Ptr<PlannedTrip>)
  }
  impl PartialOrd for VolunteerToConsider {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
      // let my_day = self.try_allocate_slot.start.date();
      // let other_day = other.try_allocate_slot.start.date();
      let my_day;
      let other_day;
      use std::cmp::Ordering::*;
      use CandidateSlot::*;
      match (&self.try_allocate_slot, &other.try_allocate_slot) {
        (NewSlot{slot_time: a, ..}, NewSlot{slot_time: b, ..}) => {
          my_day = a.start.date();
          other_day = b.start.date();
        },
        (Reuse(a), Reuse(b)) => {
          my_day = a.time_start.date();
          other_day = b.time_start.date();
        },
        (NewSlot {..}, Reuse {..}) => {
          return Some(Less);
        },
        (Reuse(_), NewSlot {..}) => {
          return Some(Greater);
        }
      }
      Some(match my_day.cmp(&other_day) {
        Less => Greater,
        Greater => Less,
        Equal => {
          self.time_taken.cmp(&other.time_taken).reverse()
        }
      })
    }
  }
  impl Ord for VolunteerToConsider {
    fn cmp(&self, other: &Self) -> Ordering {
      self.partial_cmp(other).unwrap()
    }
  }
  impl PartialEq for VolunteerToConsider {
    fn eq(&self, other: &Self) -> bool {
      self.partial_cmp(&other).unwrap() == std::cmp::Ordering::Equal
    }
  }
  impl Eq for VolunteerToConsider {}
  let mut volunteer_to_consider = BinaryHeap::new();
  let now = server.time_now();
  let vauge_tomorrow = if now.hour() < 6 {
    now.date()
  } else {
    now.date() + Duration::days(1)
  };
  let search_time_range = now..(vauge_tomorrow.and_time(NaiveTime::from_hms(0, 0, 0)) + Duration::days(if is_urgent { 2 } else { 7 }));
  let mut store_open_time: DisjointIntervals<NaiveDateTime, Range<NaiveDateTime>> = DisjointIntervals::new();
  {
    let mut curr_day = search_time_range.start.date();
    while curr_day <= search_time_range.end.date() {
      let today_open_time = &store.opening_time[curr_day.weekday().num_days_from_monday() as usize];
      store_open_time.insert(curr_day.and_time(today_open_time.start)..curr_day.and_time(today_open_time.end)).unwrap();
      curr_day += Duration::days(1);
    }
  }
  fn trip_can_be_reused(
    existing_trips: &PlannedTrip,
    vulnerable_point: (f32, f32),
    store: &Ptr<Store>,
    my_item_total_weight: u32,
    order_is_urgent: bool
  ) -> bool {
    if existing_trips.overall_state != OrderState::Pending {
      return false;
    }
    if existing_trips.store.as_ptr() != store.as_ptr() {
      return false;
    }
    if existing_trips.deliveries.len() >= 3 {
      return false;
    }
    if !order_is_urgent && existing_trips.delivered_by.reserved_time.intersects_interval(existing_trips.as_interval()) {
      return false;
    }
    if existing_trips.deliveries.iter().map(|x| meter_distance_between(x.deliver_to.as_point(), vulnerable_point)).max_by(f32::total_cmp).unwrap() > 100f32 {
      return false;
    }
    if existing_trips.deliveries.iter().map(|x| x.total_weight).sum::<u32>() + my_item_total_weight > existing_trips.delivered_by.max_weight_grams {
      return false;
    }
    true
  }
  fn probe_volunteer(
    server: &Server,
    vol: Ptr<Volunteer>,
    queue: &mut BinaryHeap<VolunteerToConsider>,
    search_time_range: &Range<NaiveDateTime>,
    is_urgent: bool,
    vulnerable_point: (f32, f32),
    store_point: (f32, f32),
    store_open_time: &DisjointIntervals<NaiveDateTime, Range<NaiveDateTime>>,
  ) {
    let l = vol.lock.lock().unwrap();
    let earliest_unallocated = {
      let mut used_ints: DisjointIntervals<NaiveDateTime, Range<NaiveDateTime>> = DisjointIntervals::new();
      for i in vol.planned_trips.iter_intersected(search_time_range.clone()) {
        used_ints.insert(i.as_interval()).unwrap();
      }
      if !is_urgent {
        let mut reserved_ints = DisjointIntervals::new();
        for i in vol.reserved_time.iter_intersected(search_time_range.clone()) {
          reserved_ints.insert(i.clone()).unwrap();
        }
        used_ints = used_ints.union(&reserved_ints);
      }
      let mut available_ints = DisjointIntervals::try_from_iter(vol.work_time.iter_intersected(search_time_range.clone()).cloned()).unwrap().intersection(&DisjointIntervals::try_from_iter(vec![search_time_range.clone()]).unwrap());
      available_ints = available_ints.difference(&used_ints);
      available_ints = available_ints.intersection(store_open_time);
      let mut ret = None;
      for int in available_ints.iter_all() {
        let trip_just_before = vol.planned_trips.iter_intersected((int.start - Duration::seconds(1))..int.start).last();
        let starting_point = match trip_just_before {
          None => vol.as_point(),
          Some(trip) => trip.deliveries.last().unwrap().deliver_to.as_point()
        };
        let dur =
            server.expected_cycle_time_between_two_points(starting_point, store_point) +
            Duration::minutes(5) +
            server.expected_cycle_time_between_two_points(store_point, vulnerable_point) +
            Duration::minutes(5) +
            Duration::minutes(10);
        if int.end - int.start >= dur {
          ret = Some((int.start..(int.start + dur), starting_point));
          break;
        }
      }
      ret
    };
    if let Some((e, sp)) = earliest_unallocated {
      queue.push(VolunteerToConsider {
        time_taken: e.end - e.start,
        try_allocate_slot: CandidateSlot::NewSlot {slot_time: e, starting_point: sp},
        volunteer: vol.clone(),
      });
    }
    drop(l);
  }
  let mut might_be_too_heavy = false;
  for vol in server.volunteers_kd.nearest_iter(vulnerable_point).cloned() {
    if meter_distance_between(vol.as_point(), vulnerable_point) > 5000f32 {
      break;
    }
    if !vol.delivery_area.hit_test(vulnerable_point) {
      continue;
    }
    if vol.max_weight_grams < total_weight {
      might_be_too_heavy = true;
      continue;
    }
    for existing_trips in vol.planned_trips.iter_intersected(search_time_range.clone()) {
      if trip_can_be_reused(&*existing_trips, vulnerable_point, &store_ptr, total_weight, is_urgent) {
        volunteer_to_consider.push(VolunteerToConsider {
          time_taken: existing_trips.time_done - existing_trips.time_start,
          try_allocate_slot: CandidateSlot::Reuse(existing_trips.clone()),
          volunteer: vol.clone()
        });
      }
    }
    probe_volunteer(server, vol, &mut volunteer_to_consider, &search_time_range, is_urgent, vulnerable_point, store.address.as_point(), &store_open_time);
  }
  loop {
    let cand = volunteer_to_consider.pop();
    if cand.is_none() {
      break;
    }
    let cand = cand.unwrap();
    let nt = cand.try_allocate_slot;
    let mut _v = cand.volunteer.clone();
    let v = &mut *_v;
    let l = v.lock.lock().unwrap();
    match nt {
      CandidateSlot::NewSlot {slot_time: nt, starting_point} => {
        if !v.planned_trips.intersects_interval(nt.clone()) && (
          is_urgent || !v.reserved_time.intersects_interval(nt.clone())
        ) {
          let trip = PlannedTrip {
            delivered_by: cand.volunteer,
            store: store_ptr,
            time_start: nt.start,
            time_to_store: nt.start + server.expected_cycle_time_between_two_points(starting_point, vulnerable_point),
            deliveries: vec![to_ptr(Delivery {
              deliver_to: vulnerable,
              items: items.to_vec(),
              total_weight,
              state: OrderState::Pending,
            })],
            time_done: nt.end,
            overall_state: OrderState::Pending
          };
          let trip = to_ptr(trip);
          v.planned_trips.insert(trip.clone()).unwrap();
          drop(l);
          // todo: send order to store
          return Ok(trip);
        } else {
          drop(l);
          if nt.end < search_time_range.end {
            probe_volunteer(server, cand.volunteer.clone(), &mut volunteer_to_consider, &(nt.end..search_time_range.end), is_urgent, vulnerable_point, store.address.as_point(), &store_open_time);
          }
        }
      },
      CandidateSlot::Reuse(mut existing_trip) => {
        if trip_can_be_reused(&*existing_trip, vulnerable_point, &store_ptr, total_weight, is_urgent) {
          existing_trip.deliveries.push(to_ptr(Delivery {
            deliver_to: vulnerable,
            items: items.to_vec(),
            total_weight,
            state: OrderState::Pending
          }));
          drop(l);
          // todo: send order to store
          return Ok(existing_trip);
        } else {
          drop(l);
        }
      }
    }
  }
  if might_be_too_heavy {
    rollback(&rollback_log, store_inv);
    return Err("Order is too large to be delivered by one volunteer.");
  }

  // struct MakeSpacePlan {}

  // fn try_make_space(
  //   volunteer: Ptr<Volunteer>,
  //   search_space: DisjointIntervals<NaiveDateTime, Range<NaiveDateTime>>,
  //   store_open_time: DisjointIntervals<NaiveDateTime, Range<NaiveDateTime>>,
  //   depth_limit: u32,
  // ) -> Option<MakeSpacePlan> {
  //   unimplemented!()
  //   /*
  //     * Probe current volunteer.
  //       * If it has space, return that.
  //       * Otherwise, if `depth_limit` == 0, return None.
  //       * Otherwise, continue.
  //     * Iterate through the orders of the current volunteer in `search_space`.
  //       * Make priority queue `candidate_spaces`, sorted with cost.
  //       * For each order, probe its suitable volunteers.
  //         * For each probed volunteer, call try_make_space on it, decreasing `depth_limit` by 1.
  //           * If successful, and that the volunteer is able to deliver to all vulnerables in the order,
  //             add the space to the queue.
  //       * If queue is not empty:
  //         * Use the best space and extend it to form the return value
  //         * Return that
  //       * Otherwise, return None.
  //    */
  // }
  // todo: try_make_space

  rollback(&rollback_log, store_inv);
  Err("Unable to match.")
}

pub fn daily_cron(_server: &mut Server) {
  // todo: rebuild kbtree every midnight
  // todo: rearrange tomorrow's scheduled trips to get the least distance.
}
