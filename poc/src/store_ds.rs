use crate::{Address, AsPoint, BTreeMap, Mutex, NaiveTime};
use std::ops::Range;

#[derive(AsPoint, Debug)]
pub struct Store {
  pub name: String,
  #[point] pub address: Address,
  /// Name -> Item
  pub inventory: BTreeMap<String, Item>,
  /// Mon, Tue, ..., Sat, Sun
  pub opening_time: [Range<NaiveTime>; 7]
}

#[derive(Debug)]
pub struct Item {
  pub name: String,
  pub weight_grams: u32,
  pub amount_left: Mutex<u32>,
}

impl Clone for Item {
  fn clone(&self) -> Self {
    Self {
      name: self.name.clone(),
      weight_grams: self.weight_grams,
      amount_left: Mutex::new(*self.amount_left.lock().unwrap())
    }
  }
}
