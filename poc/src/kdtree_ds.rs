use std::ops::Deref;

use crate::Ptr;

pub trait AsPoint {
  /// Return (lat, lon) in degrees.
  fn as_point(&self) -> (f32, f32);
}

impl AsPoint for (f32, f32) {
  fn as_point(&self) -> (f32, f32) {
    *self
  }
}

impl<T: AsPoint> AsPoint for Ptr<T> {
  fn as_point(&self) -> (f32, f32) {
    self.deref().as_point()
  }
}

pub struct KdTree<T: AsPoint> {
  inner: kdtree::KdTree<f32, Option<T>, [f32; 2]>,
}

impl<T: AsPoint> Default for KdTree<T> {
  fn default() -> Self {
    Self::new()
  }
}

impl<T: AsPoint> KdTree<T> {
  pub fn new() -> Self {
    Self {
      inner: kdtree::KdTree::new(2)
    }
  }

  pub fn nearest_iter<'a>(&'a self, point: (f32, f32)) -> impl Iterator<Item = &'a T> + 'a {
    self.inner.iter_nearest(Box::leak(vec![point.0, point.1].into_boxed_slice()), &dist_fn).unwrap().filter_map(|(_, x)| {
      x.as_ref()
    })
  }

  pub fn insert(&mut self, item: T) {
    let point = item.as_point();
    self.inner.add([point.0, point.1], Some(item)).unwrap();
  }
}

impl<T: AsPoint + PartialEq + Eq> KdTree<T> {
  pub fn remove(&mut self, item: &T) {
    let point = item.as_point();
    for (_, it) in self.inner.iter_nearest_mut(&[point.0, point.1], &dist_fn).unwrap() {
      if it.is_none() || it.as_ref().unwrap() != item {
        continue;
      }
      *it = None;
      return;
    }
  }
}

impl<T: AsPoint> KdTree<T> {
  pub fn rebuild(self) -> KdTree<T> {
    unimplemented!()
  }
}

pub fn meter_distance_between(a: (f32, f32), b: (f32, f32)) -> f32 {
  const _EARTH_RADIUS: f32 = 6371000f32;
  // todo
  ((a.0 - b.0).powi(2) + (a.1 - b.1).powi(2)).sqrt() * 100f32
}

fn dist_fn(a: &[f32], b: &[f32]) -> f32 {
  assert_eq!(a.len(), 2);
  assert_eq!(b.len(), 2);
  meter_distance_between((a[0], a[1]), (b[0], b[1]))
}
