use std::ops::Range;

use crate::*;
use std::fmt::{Debug, Formatter};

#[derive(Copy, Clone, Debug)]
pub enum ScryptData {
  Opaque
}
impl ScryptData {
  pub fn verify(&self, _password: &str) -> bool {
    unimplemented!()
  }
}

#[derive(Debug)]
pub struct LoginCredential {
  pub email: String,
  pub password: ScryptData,
  pub authorisation: CredentialAuthorisation,
}

impl LoginCredential {
  pub fn unwrap_volunteer(&self) -> Result<Ptr<Volunteer>, &'static str> {
    if let CredentialAuthorisation::Volunteer(vol) = &self.authorisation {
      Ok(vol.clone())
    } else {
      Err("Unauthorized login.")
    }
  }

  pub fn unwrap_vulnerable(&self) -> Result<Ptr<Vulnerable>, &'static str> {
    if let CredentialAuthorisation::Vulnerable(vul) = &self.authorisation {
      Ok(vul.clone())
    } else {
      Err("Unauthorized login.")
    }
  }

  pub fn unwrap_store(&self) -> Result<Ptr<Store>, &'static str> {
    if let CredentialAuthorisation::StoreAdmin(sa) = &self.authorisation {
      Ok(sa.clone())
    } else {
      Err("Unauthorized login.")
    }
  }

  pub fn unwrap_council(&self) -> Result<Ptr<LocalCouncil>, &'static str> {
    if let CredentialAuthorisation::LocalCouncil(lc) = &self.authorisation {
      Ok(lc.clone())
    } else {
      Err("Unauthorized login.")
    }
  }
}

#[derive(Debug, Clone)]
pub enum CredentialAuthorisation {
  StoreAdmin(Ptr<Store>),
  Vulnerable(Ptr<Vulnerable>),
  Volunteer(Ptr<Volunteer>),
  LocalCouncil(Ptr<LocalCouncil>),
}

#[derive(Clone, Debug)]
pub struct Address {
  pub text: String,
  /// in degree
  pub lat: f32,
  /// in degree
  pub lon: f32
}

impl AsPoint for Address {
  fn as_point(&self) -> (f32, f32) {
    (self.lat, self.lon)
  }
}

#[derive(Debug, Clone, AsPoint)]
pub struct Vulnerable {
  pub name: String,
  #[point] pub home_address: Address,
  pub phone_number: String,
  pub payment_methods: Vec<PaymentCard>
}

#[derive(AsPoint, Debug)]
pub struct Volunteer {
  pub lock: Mutex<()>,
  pub name: String,
  #[point] pub home_address: Address,
  pub phone_number: String,
  pub planned_trips: DisjointIntervals<NaiveDateTime, Ptr<PlannedTrip>>,
  pub work_time: DisjointIntervals<NaiveDateTime, Range<NaiveDateTime>>,
  pub reserved_time: DisjointIntervals<NaiveDateTime, Range<NaiveDateTime>>,
  /// This does not limit which store the volunteer may need to go to.
  pub delivery_area: RegionOfSpace,
  pub max_weight_grams: u32,
}

#[derive(Debug)]
pub struct LocalCouncil {
  pub administrative_region: RegionOfSpace,
  pub name: String
}

#[derive(Clone)]
pub struct PaymentCard {
  pub card_number: String,
  /// year, month
  pub expiry_date: (u16, u16)
}

impl Debug for PaymentCard {
  fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
    write!(f, "PaymentCard {{ ... }}")
  }
}
