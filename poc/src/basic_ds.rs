use std::any::TypeId;
use std::cell::RefCell;
use std::cell::UnsafeCell;
pub use std::collections::{BinaryHeap, BTreeMap};
use std::collections::HashSet;
use std::fmt::{Debug, Formatter};
use std::ops::{Add, AddAssign, Deref, DerefMut};
use std::rc::Rc;
pub use std::sync::Mutex;

pub use chrono::{Duration, NaiveDate, NaiveDateTime, NaiveTime};

pub struct Ptr<T> (Rc<UnsafeCell<T>>);

pub fn to_ptr<T>(v: T) -> Ptr<T> {
  Ptr(Rc::new(UnsafeCell::new(v)))
}

impl<T> Deref for Ptr<T> {
  type Target = T;

  fn deref(&self) -> &Self::Target {
    unsafe { &*self.as_ptr() }
  }
}

impl<T> DerefMut for Ptr<T> {
  fn deref_mut(&mut self) -> &mut Self::Target {
    unsafe { &mut *self.as_mut_ptr() }
  }
}

impl<T> Ptr<T> {
  pub fn as_ptr(&self) -> *const T {
    self.0.deref().get()
  }

  pub fn as_mut_ptr(&mut self) -> *mut T {
    self.0.deref().get()
  }
}

impl<T> Clone for Ptr<T> {
  fn clone(&self) -> Self {
    Self(self.0.clone())
  }
}

thread_local! {
  static DBG_PRINTED_TYPE: RefCell<HashSet<TypeId>> = RefCell::new(HashSet::new());
}

#[cfg(not(miri))]
impl<T: Debug + 'static> Debug for Ptr<T> {
  fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
    write!(f, "&")?;
    let current_type = TypeId::of::<T>();
    if DBG_PRINTED_TYPE.with(|x| x.borrow().contains(&current_type)) {
      f.write_fmt(format_args!("{}...", std::any::type_name::<T>()))?;
    } else {
      DBG_PRINTED_TYPE.with(|x| x.borrow_mut().insert(current_type));
      let err: Option<std::fmt::Error>;
      if !f.alternate() {
        err = f.write_fmt(format_args!("{:?}", self.deref())).err();
      } else {
        err = f.write_fmt(format_args!("{:#?}", self.deref())).err();
      }
      DBG_PRINTED_TYPE.with(|x| x.borrow_mut().remove(&current_type));
      if let Some(e) = err {
        return Err(e);
      }
    }
    Ok(())
  }
}
#[cfg(miri)]
impl<T: Debug + 'static> Debug for Ptr<T> {
  fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
    write!(f, "&???")
  }
}

#[derive(Debug, Clone, Default)]
pub struct RegionOfSpace {
  pub regions: Vec<Polygon>
}

#[derive(Debug, Clone, Default)]
pub struct Polygon {
  pub vertices: Vec<(f32, f32)>
}

impl RegionOfSpace {
  pub fn empty() -> Self {
    RegionOfSpace {
      regions: Vec::new()
    }
  }

  pub fn rectangle(point1: (f32, f32), point2: (f32, f32)) -> Self {
    RegionOfSpace {
      regions: vec![Polygon::rectangle(point1, point2)]
    }
  }

  pub fn hit_test(&self, point: (f32, f32)) -> bool {
    self.regions.iter().any(|p| p.hit_test(point))
  }
}

impl Polygon {
  pub fn rectangle(point1: (f32, f32), point2: (f32, f32)) -> Self {
    Polygon {
      vertices: vec![point1, (point2.0, point1.1), point2, (point1.0, point2.1)]
    }
  }

  pub fn hit_test(&self, _point: (f32, f32)) -> bool {
    // TODO
    true
  }
}

impl Add<&RegionOfSpace> for RegionOfSpace {
  type Output = Self;

  fn add(mut self, rhs: &Self) -> Self::Output {
    self.regions.extend(rhs.regions.iter().cloned());
    self
  }
}

impl AddAssign<&RegionOfSpace> for RegionOfSpace {
  fn add_assign(&mut self, rhs: &RegionOfSpace) {
    self.regions.extend(rhs.regions.iter().cloned())
  }
}
// pub use Result;
// pub use std::ops::Range;
