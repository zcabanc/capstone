use std::collections::BTreeSet;
use std::ops::{Range, Deref, DerefMut, Sub, Add};
use std::cmp::Ordering;
use crate::Ptr;
use std::mem::swap;

pub trait AsInterval {
  type IntervalNumber: Ord + Eq + Clone;
  fn as_interval(&self) -> Range<Self::IntervalNumber>;
}

impl<I: Ord + Eq + Clone> AsInterval for Range<I> {
  type IntervalNumber = I;

  fn as_interval(&self) -> Self {
    self.clone()
  }
}

impl<I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I>> AsInterval for Ptr<T> {
  type IntervalNumber = I;

  fn as_interval(&self) -> Range<Self::IntervalNumber> {
    self.deref().as_interval()
  }
}

#[derive(Debug, Clone)]
enum Wrapper<I, T> {
  Val(T),
  Flag(I),
}

impl<I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I>> Wrapper<I, T> {
  fn get_start(&self) -> I {
    use Wrapper::*;
    match self {
      Val(v) => v.as_interval().start,
      Flag(f) => f.clone()
    }
  }
}

impl<I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I>> PartialEq for Wrapper<I, T> {
  fn eq(&self, other: &Self) -> bool {
    self.get_start() == other.get_start()
  }
}

impl<I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I>> PartialOrd for Wrapper<I, T> {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.get_start().cmp(&other.get_start()))
  }
}

impl<I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I>> Eq for Wrapper<I, T> {}

impl<I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I>> Ord for Wrapper<I, T> {
  fn cmp(&self, other: &Self) -> Ordering {
    self.partial_cmp(other).unwrap()
  }
}

impl<I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I>> Wrapper<I, T> {
  fn new(inner: T) -> Wrapper<I, T> {
    Wrapper::Val(inner)
  }
}

fn flag<I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I>>(start: I) -> Wrapper<I, T> {
  Wrapper::Flag(start)
}

impl<I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I>> Deref for Wrapper<I, T> {
  type Target = T;

  fn deref(&self) -> &T {
    use Wrapper::*;
    match self {
      Val(v) => v,
      Flag(_) => unreachable!("flag dereference not allowed.")
    }
  }
}

impl<I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I>> DerefMut for Wrapper<I, T> {
  fn deref_mut(&mut self) -> &mut Self::Target {
    use Wrapper::*;
    match self {
      Val(v) => v,
      Flag(_) => unreachable!("flag dereference not allowed.")
    }
  }
}

#[derive(Debug, Clone)]
pub struct DisjointIntervals<I, T> {
  intervals: BTreeSet<Wrapper<I, T>>
}

impl<I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I>> Default for DisjointIntervals<I, T> {
  fn default() -> Self {
    Self::new()
  }
}

impl<I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I>> DisjointIntervals<I, T> {
  pub fn new() -> Self {
    Self {
      intervals: BTreeSet::new()
    }
  }

  pub fn intersects_interval(&self, interval: Range<I>) -> bool {
    if interval.start > interval.end {
      panic!("Invalid interval.");
    }
    if self.intervals.is_empty() {
      return false;
    }
    let earliest = self.intervals.first().unwrap().as_interval().start;
    if earliest >= interval.end {
      return false;
    }
    let mut iter = self.intervals.range(..flag(interval.end));
    let last_int = iter.next_back().unwrap().as_interval();
    last_int.end > interval.start
  }

  pub fn insert(&mut self, item: T) -> Result<(), &'static str> {
    let item_int = item.as_interval();
    if self.intersects_interval(item_int) {
      Err("Interval will intersect some existing intervals when inserted.")
    } else {
      self.intervals.insert(Wrapper::new(item));
      Ok(())
    }
  }

  pub fn iter_intersected(&self, interval: Range<I>) -> DisjointIntervalsIter<I, T> {
    DisjointIntervalsIter {
      start: interval.start.clone(),
      back_iter: Some(self.intervals.range(..flag(interval.start.clone()))),
      forward_iter: self.intervals.range(flag(interval.start.clone())..flag(interval.end)),
    }
  }

  pub fn iter_all(&self) -> DisjointIntervalsIterAll<I, T> {
    DisjointIntervalsIterAll {
      all_iter: self.intervals.iter()
    }
  }

  pub fn try_from_iter<Iter: IntoIterator<Item=T>>(iter: Iter) -> Result<DisjointIntervals<I, T>, &'static str> {
    let mut res = DisjointIntervals::new();
    for i in iter {
      res.insert(i)?;
    }
    Ok(res)
  }

  pub fn remove_intersected(&mut self, interval: Range<I>) {
    let intersected = self.iter_intersected(interval).map(|x| x.as_interval()).collect::<Vec<_>>();
    for int in intersected {
      self.intervals.remove(&flag(int.start));
    }
  }

  pub fn len(&self) -> usize {
    self.intervals.len()
  }

  pub fn is_empty(&self) -> bool {
    self.len() == 0
  }
}

impl<I, T, N> DisjointIntervals<I, T> where
    I: Ord + Eq + Clone + Sub<Output=N> + Add<N, Output=I>,
    T: AsInterval<IntervalNumber=I>,
    N: Ord + Eq + Clone {
  pub fn find_free_space(&mut self, interval: Range<I>, len: N) -> Option<Range<I>> {
    if interval.end.clone() - interval.start.clone() < len {
      return None;
    }
    if !self.intersects_interval(interval.start.clone()..(interval.start.clone() + len.clone())) {
      return Some(interval.start.clone()..(interval.start + len));
    }
    for existing in self.iter_intersected(interval.clone()) {
      let eint = existing.as_interval();
      if eint.end >= interval.end {
        continue;
      }
      if interval.end.clone() - eint.end.clone() >= len.clone() &&
          !self.intersects_interval(eint.end.clone()..(eint.end.clone() + len.clone())) {
        return Some(eint.end.clone()..(eint.end + len));
      }
    }
    None
  }
}

impl<I: Ord + Eq + Clone> DisjointIntervals<I, Range<I>> {
  pub fn difference(&self, rhs: &Self) -> Self {
    let mut result: Self = DisjointIntervals::new();
    'aloop: for chunka in self.iter_all() {
      let mut current_start = chunka.start.clone();
      for subtract in rhs.iter_intersected(chunka.clone()) {
        if subtract.start >= subtract.end {
          continue;
        }
        if subtract.start > current_start {
          result.insert(current_start.clone()..subtract.start.clone()).unwrap();
        }
        current_start = subtract.end.clone();
        if current_start >= chunka.end {
          continue 'aloop;
        }
      }

      if current_start < chunka.end {
        result.insert(current_start..chunka.end.clone()).unwrap();
      }
    }
    result
  }

  /// O(a log b + b)
  pub fn intersection(&self, rhs: &Self) -> Self {
    let mut result: Self = DisjointIntervals::new();
    let mut a = self;
    let mut b = rhs;
    if a.len() > b.len() {
      swap(&mut a, &mut b);
    }
    for chunka in a.iter_all() {
      if chunka.start >= chunka.end {
        continue;
      }
      for chunkb in b.iter_intersected(chunka.clone()) {
        if chunkb.start >= chunkb.end {
          continue;
        }
        debug_assert!(!(chunkb.end <= chunka.start || chunkb.start >= chunka.end));
        result.insert(chunka.start.clone().max(chunkb.start.clone())..chunkb.end.clone().min(chunka.end.clone())).unwrap();
      }
    }
    result
  }

  /// O(a + b)
  pub fn union(&self, rhs: &Self) -> Self {
    let mut aiter = self.iter_all().filter(|x| x.end > x.start).peekable();
    let mut biter = rhs.iter_all().filter(|x| x.end > x.start).peekable();
    let mut result: Vec<Range<I>> = Vec::new();
    loop {
      let anxt = aiter.next();
      let bnxt = biter.next();
      match (anxt, bnxt) {
        (None, None) => {
          break;
        },
        (Some(a), None) => {
          result.push(a.clone());
          break;
        },
        (None, Some(b)) => {
          result.push(b.clone());
          break;
        },
        (Some(a), Some(b)) => {
          let mut current_int;
          if a.end < b.start {
            // [a   )   [b   )
            result.push(a.clone());
            current_int = b.clone();
          } else if b.end < a.start {
            // [b   )   [a   )
            result.push(b.clone());
            current_int = a.clone();
          } else {
            // a and b overlaps
            current_int = (a.start.clone().min(b.start.clone()))..(a.end.clone().max(b.end.clone()));
          }
          loop {
            if let Some(a) = aiter.peek() {
              if a.start <= current_int.end {
                current_int.end = current_int.end.max(aiter.next().unwrap().end.clone());
                continue;
              }
            }
            if let Some(b) = biter.peek() {
              if b.start <= current_int.end {
                current_int.end = current_int.end.max(biter.next().unwrap().end.clone());
                continue;
              }
            }
            break;
          }
          result.push(current_int);
        }
      }
    }
    Self::try_from_iter(result).unwrap()
  }
}

pub struct DisjointIntervalsIter<'a, I, T: 'a> {
  start: I,
  back_iter: Option<std::collections::btree_set::Range<'a, Wrapper<I, T>>>,
  forward_iter: std::collections::btree_set::Range<'a, Wrapper<I, T>>,
}

impl<'a, I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I> + 'a> Iterator for DisjointIntervalsIter<'a, I, T> {
  type Item = &'a T;

  fn next(&mut self) -> Option<&'a T> {
    let mut ret = None;
    if let Some(back_iter) = self.back_iter.as_mut() {
      if let Some(back_i) = back_iter.next_back() {
        if back_i.as_interval().end > self.start {
          ret = Some(back_i.deref());
        }
      }
      self.back_iter = None;
    }
    if let Some(ret) = ret {
      Some(ret)
    } else {
      self.forward_iter.next().map(|x| x.deref())
    }
  }
}

pub struct DisjointIntervalsIterAll<'a, I, T: 'a> {
  all_iter: std::collections::btree_set::Iter<'a, Wrapper<I, T>>
}

impl<'a, I: Ord + Eq + Clone, T: AsInterval<IntervalNumber=I> + 'a> Iterator for DisjointIntervalsIterAll<'a, I, T> {
  type Item = &'a T;

  fn next(&mut self) -> Option<&'a T> {
    self.all_iter.next().map(|x| x.deref())
  }
}

#[test]
fn test() {
  let mut dj: DisjointIntervals<i32, Range<i32>> = DisjointIntervals::new();
  assert_eq!(dj.iter_all().count(), 0);
  assert_eq!(dj.iter_intersected(2..3).count(), 0);
  assert_eq!(dj.intersects_interval(2..3), false);
  dj.insert(1..4).unwrap();
  assert_eq!(dj.iter_all().count(), 1);
  assert_eq!(dj.iter_intersected(2..3).count(), 1);
  assert_eq!(dj.intersects_interval(2..3), true);
  assert_eq!(dj.iter_intersected(2..5).count(), 1);
  assert_eq!(dj.intersects_interval(2..5), true);
  assert_eq!(dj.iter_intersected(1..5).count(), 1);
  assert_eq!(dj.intersects_interval(1..5), true);
  assert_eq!(dj.iter_intersected(0..1).count(), 0);
  assert_eq!(dj.intersects_interval(0..1), false);
  assert_eq!(dj.iter_intersected(4..5).count(), 0);
  assert_eq!(dj.intersects_interval(4..5), false);

  assert_eq!(dj.iter_intersected(1..5).count(), 1);
  assert_eq!(dj.intersects_interval(1..5), true);
  dj.remove_intersected(3..5);
  assert_eq!(dj.iter_intersected(1..5).count(), 0);
  assert_eq!(dj.intersects_interval(1..5), false);
}

#[allow(unused_macros)]
macro_rules! int {
    ($i:ident, $($r:expr),*) => {
      let mut $i: DisjointIntervals<i32, Range<i32>> = DisjointIntervals::new();
      $(
        $i.insert($r).unwrap();
      )+
    };
}
#[allow(unused_macros)]
macro_rules! test {
    ($actual:expr, $($expected_r:expr),*) => {
      let expected: Vec<Range<i32>> = vec![$($expected_r),*];
      assert_eq!(&$actual.iter_all().cloned().collect::<Vec<Range<i32>>>(), &expected);
    };
}

#[test]
fn test_setops() {
  /*
          0------1------2------3------4------5------6
      a:  [             )
     a1:         [)
     a2:                [)
      b:         [      )
      c:         [             )
      d:                [      )
      e:                              [      )
     be:         [      )             [      )
      f:  [      )             [      )
      g:         [      )                    [     )
   */
  int!(a, 0..2);
  int!(a1, 1..1);
  int!(a2, 2..2);
  int!(b, 1..2);
  int!(c, 1..3);
  int!(d, 2..3);
  int!(e, 4..5);
  int!(be, 1..2, 4..5);
  int!(f, 0..1, 3..4);
  int!(g, 1..2, 5..6);

  test!(a.union(&a), 0..2);
  test!(a.union(&b), 0..2);
  test!(a.union(&c), 0..3);
  test!(a.union(&d), 0..3);
  test!(a.union(&e), 0..2, 4..5);
  test!(b.union(&e), 1..2, 4..5);
  test!(f.union(&g), 0..2, 3..4, 5..6);

  test!(a.difference(&b), 0..1);
  test!(a.difference(&c), 0..1);
  test!(c.difference(&a), 2..3);
  test!(c.difference(&b), 2..3);
  test!(a.difference(&a1), 0..2);
  test!(a.difference(&a2), 0..2);
  test!(a.difference(&d), 0..2);
  test!(a.difference(&e), 0..2);
  test!(a.difference(&e), 0..2);
  test!(a.difference(&be), 0..1);
  test!(b.difference(&a), );

  test!(f.intersection(&g), );
  test!(f.intersection(&a), 0..1);
  test!(a.intersection(&b), 1..2);
  test!(b.intersection(&a), 1..2);
  test!(be.intersection(&f), );
  test!(f.intersection(&be), );

  test!(a.intersection(&a1), );
  test!(a.intersection(&a2), );
}

#[test]
fn test_union_merging() {
  /*
          0------1------2------3------4------5------6
      a:  [      )
      b:         [             )
      c:                [      )
      d:                [             )
      e:                       [      )
      f:                [      )
      g:                                     [      )
      h:                       [             )
      i:                                            [)
   */
  int!(a, 0..1);
  int!(b, 1..3);
  int!(c, 2..3);
  int!(d, 2..4);
  int!(e, 3..4);
  int!(f, 2..3);
  int!(g, 5..6);
  int!(h, 3..5);
  int!(i, 6..6);

  test!(a.union(&b), 0..3);
  test!(b.union(&c), 1..3);
  test!(c.union(&d), 2..4);
  test!(d.union(&e), 2..4);
  test!(e.union(&f), 2..4);
  test!(f.union(&g), 2..3, 5..6);
  test!(g.union(&h), 3..6);
  test!(h.union(&i), 3..5);
  test!(g.union(&i), 5..6);
  test!(f.union(&h).union(&g), 2..6);
  test!(f.union(&g).union(&h), 2..6);
  test!(h.union(&f).union(&g), 2..6);
  test!(h.union(&g).union(&f), 2..6);
  test!(g.union(&h).union(&f), 2..6);
  test!(g.union(&f).union(&h), 2..6);

  test!(a.union(&b).union(&c).union(&d).union(&e).union(&f).union(&g).union(&h).union(&i), 0..6);
  test!(i.union(&h).union(&g).union(&f).union(&e).union(&d).union(&c).union(&b).union(&a), 0..6);
  test!(a.union(&b.union(&c.union(&d.union(&e.union(&f.union(&g.union(&h.union(&i)))))))), 0..6);
  test!(i.union(&h.union(&g.union(&f.union(&e.union(&d.union(&c.union(&b.union(&a)))))))), 0..6);
}
