use crate::*;
use std::ops::Range;

#[derive(Debug)]
pub struct PlannedTrip {
  pub delivered_by: Ptr<Volunteer>,
  pub store: Ptr<Store>,
  pub time_start: NaiveDateTime,
  pub time_to_store: NaiveDateTime,
  pub deliveries: Vec<Ptr<Delivery>>,
  /// time_to_store + duration_of_first_delivery + reserved_more_time
  /// Won't change after this PlannedTrip is created.
  pub time_done: NaiveDateTime,
  pub overall_state: OrderState
}

#[derive(Debug)]
pub struct Delivery {
  pub deliver_to: Ptr<Vulnerable>,
  pub items: Vec<(String, u32)>,
  pub total_weight: u32,
  pub state: OrderState,
}

impl AsInterval for PlannedTrip {
  type IntervalNumber = NaiveDateTime;

  fn as_interval(&self) -> Range<NaiveDateTime> {
    self.time_start..self.time_done
  }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum OrderState {
  Pending,
  Collected,
  Delivered,
  Cancelled
}
