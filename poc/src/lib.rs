#![feature(map_first_last,total_cmp)]

use derive::AsPoint;

pub use basic_ds::*;
pub use kdtree_ds::*;
pub use disjoint_intervals::*;
pub use order_ds::*;
pub use store_ds::*;
pub use tasks::*;
pub use user_ds::*;
use chrono::{DateTime, Local};
use std::time::SystemTime;
use std::ops::{Range, Add};

mod basic_ds;
mod kdtree_ds;
mod disjoint_intervals;
mod user_ds;
mod order_ds;
mod store_ds;
mod tasks;

pub fn day_time_range(day: NaiveDate) -> Range<NaiveDateTime> {
  day.and_time(NaiveTime::from_hms(0,0,0))..(day.add(Duration::days(1)).and_time(NaiveTime::from_hms(0,0,0)))
}

pub struct Server {
  pub credentials: BTreeMap<String, Ptr<LoginCredential>>,
  pub stores_kd: KdTree<Ptr<Store>>,
  pub volunteers_kd: KdTree<Ptr<Volunteer>>,
  pub fake_time: Option<NaiveDateTime>
}

impl Default for Server {
  fn default() -> Self {
    Self::new()
  }
}

impl Server {
  pub fn new() -> Self {
    Self {
      credentials: BTreeMap::new(),
      stores_kd: KdTree::new(),
      volunteers_kd: KdTree::new(),
      fake_time: None
    }
  }

  pub fn find_credential(&self, email: &str) -> Option<Ptr<LoginCredential>> {
    self.credentials.get(email).cloned()
  }

  #[cfg(not(miri))]
  pub fn time_now(&self) -> NaiveDateTime {
    if let Some(ft) = self.fake_time {
      ft
    } else {
      let now: DateTime<Local> = SystemTime::now().into();
      now.naive_local()
    }
  }

  #[cfg(miri)]
  pub fn time_now(&self) -> NaiveDateTime {
    if let Some(ft) = self.fake_time {
      ft
    } else {
      NaiveDateTime::from_timestamp(0, 0)
    }
  }

  pub fn date_now(&self) -> NaiveDate {
    self.time_now().date()
  }

  pub fn expected_cycle_time_between_two_points(&self, point1: (f32, f32), point2: (f32, f32)) -> Duration {
    // todo
    let _ = Duration::milliseconds(1) * (meter_distance_between(point1, point2) * 250f32) as i32;
    Duration::minutes(20)
  }
}
