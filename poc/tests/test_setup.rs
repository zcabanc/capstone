use poc::*;
use chrono::Timelike;
use std::ops::Range;

#[derive(Default)]
pub struct Setup {
  pub server: Server,
  pub volunteers: Vec<(Ptr<Volunteer>, Ptr<LoginCredential>)>,
  pub vulnerable: Vec<(Ptr<Vulnerable>, Ptr<LoginCredential>)>,
  pub stores: Vec<(Ptr<Store>, Ptr<LoginCredential>)>,
  pub councils: Vec<(Ptr<LocalCouncil>, Ptr<LoginCredential>)>,
}

pub struct TestOrder<'a> {
  pub setup: &'a mut Setup,
  pub vulnerable: Option<(Ptr<Vulnerable>, Ptr<LoginCredential>)>,
  pub store: Option<Ptr<Store>>,
  pub items: Vec<(String, u32)>,
  pub is_urgent: bool,
}

fn parse_wt(wt: &[((u32, u32), (u32, u32))], now: NaiveDateTime) -> Vec<Range<NaiveDateTime>> {
  let mut start_date = now.date();
  if now.hour() > 0 || now.minute() > 0 {
    start_date += Duration::days(1);
  }
  let mut chunks = Vec::new();
  let mut last_to: Option<NaiveTime> = None;
  let mut curr_date = start_date;
  for c in wt {
    if (c.0).0 == 24 {
      curr_date += Duration::days(1);
      continue;
    }
    let from = NaiveTime::from_hms((c.0).0, (c.0).1, 0);
    let to = NaiveTime::from_hms((c.1).0, (c.1).1, 0);
    if let Some(lt) = last_to {
      if lt > from {
        curr_date += Duration::days(1);
      }
    }
    last_to = Some(to);
    chunks.push(curr_date.and_time(from)..curr_date.and_time(to));
  }
  chunks
}

impl Setup {
  pub fn new() -> Self {
    Self::default()
  }

  pub fn fake_time(&mut self, time: NaiveDateTime) -> &mut Self {
    self.server.fake_time = Some(time);
    self
  }

  pub fn add_volunteer(&mut self, id: &str, point: (f32, f32)) -> &mut Self {
    let vol = add_volunteer(
      &mut self.server,
      id.to_owned(),
      Address {
        text: format!("Home of {}", id),
        lat: point.0,
        lon: point.1,
      },
      "00000000".to_owned(),
      format!("{}@vol.org", id),
      ScryptData::Opaque,
    );
    let auth = self.server.find_credential(&format!("{}@vol.org", id)).unwrap();
    self.volunteers.push((vol, auth));
    self
  }

  pub fn and_set_worktime(&mut self, wt: &[((u32, u32), (u32, u32))]) -> &mut Self {
    let (vol, vol_auth) = self.volunteers.last().unwrap();
    let now = self.server.time_now();
    volunteer_set_worktime(
      &mut self.server, vol_auth.clone(),
      parse_wt(wt, now),
    ).unwrap();
    vol.clone().reserved_time = DisjointIntervals::new();
    self
  }

  pub fn and_reserve(&mut self, wt: &[((u32, u32), (u32, u32))]) -> &mut Self {
    let (vol, _) = self.volunteers.last().unwrap();
    let mut vol = vol.clone();
    let now = self.server.time_now();
    let reserve_chunks = parse_wt(wt, now);
    for c in reserve_chunks {
      vol.reserved_time.insert(c).unwrap();
    }
    self
  }

  pub fn and_add_delivery_region(&mut self, rectangle: ((f32, f32), (f32, f32))) -> &mut Self {
    let (vol, _) = self.volunteers.last().unwrap();
    let mut vol = vol.clone();
    vol.delivery_area += &RegionOfSpace::rectangle(rectangle.0, rectangle.1);
    self
  }

  pub fn with_council(&mut self, name: &str, rectangle: ((f32, f32), (f32, f32))) -> &mut Self {
    let council = add_council(
      &mut self.server,
      name.to_owned(),
      format!("{}@cil.org", name),
      ScryptData::Opaque,
      RegionOfSpace::rectangle(rectangle.0, rectangle.1)
    );
    let auth = self.server.find_credential(&format!("{}@cil.org", name)).unwrap();
    self.councils.push((council, auth));
    self
  }

  pub fn add_vulnerable(&mut self, id: &str, point: (f32, f32)) -> &mut Self {
    let council = self.councils.last().unwrap();
    let vul = add_vulnerable(
      &mut self.server,
      council.1.clone(),
      Vulnerable {
        name: id.to_owned(),
        home_address: Address {
          text: format!("Home of {}", id),
          lat: point.0,
          lon: point.1,
        },
        phone_number: "00000000".to_owned(),
        payment_methods: vec![PaymentCard {
          card_number: "xxx".to_owned(),
          expiry_date: (0, 0)
        }]
      },
      format!("{}@vul.org", id),
      ScryptData::Opaque,
    ).unwrap();
    let auth = self.server.find_credential(&format!("{}@vul.org", id)).unwrap();
    self.vulnerable.push((vul, auth));
    self
  }

  pub fn with_store(&mut self, id: &str, point: (f32, f32)) -> &mut Self {
    let store = register_store(
      &mut self.server,
      id.to_owned(),
      format!("{}@store.org", id),
      ScryptData::Opaque,
      Address {
        text: format!("Store {}", id),
        lat: point.0,
        lon: point.1,
      },
      {
        let zero_to_zero = NaiveTime::from_hms(0, 0, 0)..NaiveTime::from_hms(23, 59, 59);
        [zero_to_zero.clone(), zero_to_zero.clone(), zero_to_zero.clone(), zero_to_zero.clone(), zero_to_zero.clone(), zero_to_zero.clone(), zero_to_zero.clone()]
      },
    );
    let auth = self.server.find_credential(&format!("{}@store.org", id)).unwrap();
    self.stores.push((store, auth));
    self
  }

  pub fn add_item(&mut self, name: &str, weight: u32, amount: u32) -> &mut Self {
    let (_, store_auth) = self.stores.last().unwrap();
    add_inventory(
      &mut self.server,
      store_auth.clone(),
      Item {
        name: name.to_owned(),
        weight_grams: weight,
        amount_left: Mutex::new(amount),
      },
    ).unwrap();
    self
  }

  pub fn test_order(&mut self, by_vulnerable: &str, from_store: &str) -> TestOrder {
    let vul = self.vulnerable.iter().find(|x| x.0.name == by_vulnerable).unwrap().clone();
    let store = self.stores.iter().find(|x| x.0.name == from_store).unwrap().0.clone();
    TestOrder {
      setup: self,
      vulnerable: Some(vul),
      store: Some(store),
      items: vec![],
      is_urgent: false,
    }
  }
}

impl<'a> TestOrder<'a> {
  pub fn urgent(&mut self) -> &mut Self {
    self.is_urgent = true;
    self
  }

  pub fn add_item(&mut self, name: &str, amount: u32) -> &mut Self {
    self.items.push((name.to_owned(), amount));
    self
  }

  pub fn place(&mut self) -> Result<Ptr<PlannedTrip>, &'static str> {
    let ret = place_order(
      &mut self.setup.server,
      self.vulnerable.as_ref().unwrap().1.clone(),
      self.store.as_ref().unwrap().clone(),
      &self.items,
      self.is_urgent,
    );
    ret
  }

  pub fn place_and_expect_success(&mut self) -> Ptr<PlannedTrip> {
    let vulnerable = self.vulnerable.as_ref().unwrap().0.clone();
    let planned_trip = self.place().unwrap();
    println!(
      "{vulnerable_name}: placed order; received by {volunteer} and planned on {time:?} -> {time_done:?}",
      vulnerable_name = vulnerable.name,
      volunteer = planned_trip.delivered_by.name,
      time = planned_trip.time_start,
      time_done = planned_trip.time_done.time()
    );
    planned_trip
  }

  pub fn place_and_expect_volunteer(&mut self, vol_id: &str) -> Ptr<PlannedTrip> {
    let t = self.place_and_expect_success();
    assert_eq!(&t.delivered_by.name, vol_id);
    t
  }

  pub fn place_and_expect_fail(&mut self) {
    let t = self.place();
    if let Ok(t) = t {
      panic!(
        "Expected order by {} to fail, but did not, and instead planned on {:?} by {}",
        self.vulnerable.as_ref().unwrap().0.name,
        t.time_start,
        t.delivered_by.name
      )
    }
  }
}
