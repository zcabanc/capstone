use poc::*;
use std::ops::Add;

#[test]
fn test() {
  let mut server = Server::new();
  let lc = add_council(
    &mut server,
    "My Council".to_owned(),
    "m@maowtm.org".to_owned(),
    ScryptData::Opaque,
    RegionOfSpace::rectangle((-1f32, -1f32), (1f32, 1f32))
  );
  let lc_auth = server.find_credential("m@maowtm.org").unwrap();
  let st_opening_time = {
    let t = NaiveTime::from_hms(7, 0, 0);
    let u = NaiveTime::from_hms(17, 0, 0);
    let r = t..u;
    [r.clone(), r.clone(), r.clone(), r.clone(), r.clone(), r.clone(), r.clone()]
  };
  let st = register_store(
    &mut server,
    "Sainsburys".to_owned(),
    "sainsburys@maowtm.org".to_owned(),
    ScryptData::Opaque,
    Address {
      text: "aaa".to_owned(),
      lat: 0f32,
      lon: 0f32,
    },
    st_opening_time.clone(),
  );
  let st_auth = server.find_credential("sainsburys@maowtm.org").unwrap();
  let st2 = register_store(
    &mut server,
    "Tesco".to_owned(),
    "tesco@maowtm.org".to_owned(),
    ScryptData::Opaque,
    Address {
      text: "tesco".to_owned(),
      lat: -1f32,
      lon: -2f32,
    },
    st_opening_time.clone(),
  );
  let st2_auth = server.find_credential("tesco@maowtm.org").unwrap();
  let mut vol = add_volunteer(
    &mut server,
    "Maowtm".to_owned(),
    Address {
      text: "vol_home".to_owned(),
      lat: 1f32,
      lon: 0f32,
    },
    "0000".to_owned(),
    "volunteer@maowtm.org".to_owned(),
    ScryptData::Opaque,
  );
  let vol_auth = server.find_credential("volunteer@maowtm.org").unwrap();
  let vul = add_vulnerable(
    &mut server,
    lc_auth,
    Vulnerable {
      name: "mmmm".to_owned(),
      home_address: Address {
        text: "vul_home".to_owned(),
        lat: 1f32,
        lon: 1f32,
      },
      phone_number: "0000".to_owned(),
      payment_methods: vec![PaymentCard {
        card_number: "xxx".to_owned(),
        expiry_date: (0, 0)
      }]
    },
    "vulnerable@maowtm.org".to_owned(),
    ScryptData::Opaque,
  ).unwrap();
  let vul_auth = server.find_credential("vulnerable@maowtm.org").unwrap();
  assert_eq!(
    vulnerable_search_nearest_stores(&mut server, vul_auth.clone(), 2).unwrap().into_iter().map(|x| x.name.clone()).collect::<Vec<_>>(),
    vec!["Sainsburys", "Tesco"]
  );
  let next_day = server.date_now().add(Duration::days(1));
  macro_rules! h {
      ($h:expr) => {
        next_day.and_time(NaiveTime::from_hms($h,0,0))
      };
  }
  volunteer_set_worktime(
    &mut server,
    vol_auth,
    vec![h!(9)..h!(12), h!(13)..h!(16)],
  ).unwrap();
  dbg!(&vol.reserved_time.iter_all().collect::<Vec<_>>());
  add_inventory(
    &mut server,
    st_auth.clone(),
    Item {
      name: "Milk".to_owned(),
      weight_grams: 500,
      amount_left: Mutex::new(1),
    },
  ).unwrap();
  add_inventory(
    &mut server,
    st_auth.clone(),
    Item {
      name: "Eggs".to_owned(),
      weight_grams: 200,
      amount_left: Mutex::new(1),
    },
  ).unwrap();
  add_inventory(
    &mut server,
    st2_auth.clone(),
    Item {
      name: "Meds".to_owned(),
      weight_grams: 20,
      amount_left: Mutex::new(1),
    },
  ).unwrap();
  vol.delivery_area = RegionOfSpace::rectangle((-0.5, -0.5), (0.5, 0.5));
  place_order(
    &mut server,
    vul_auth.clone(),
    st.clone(),
    &vec![
      ("Milk".to_owned(), 1)
    ],
    false
  ).unwrap();
  place_order(
    &mut server,
    vul_auth.clone(),
    st.clone(),
    &vec![
      ("Eggs".to_owned(), 1)
    ],
    false
  ).unwrap();
  place_order(
    &mut server,
    vul_auth.clone(),
    st.clone(),
    &vec![
      ("Eggs".to_owned(), 1)
    ],
    false
  ).expect_err("expected not enough items left.");
  dbg!(&vol);
}
