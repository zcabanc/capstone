use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

use test_setup::*;

mod test_setup;

fn construct_setup(wt1: &[((u32, u32), (u32, u32))], wt2: &[((u32, u32), (u32, u32))]) -> Setup {
  let mut setup = Setup::new();
  setup
      .fake_time(NaiveDateTime::new(NaiveDate::from_ymd(2020, 1, 1), NaiveTime::from_hms(0, 0, 0)))
      .with_council("London", ((0.0, 0.0), (10.0, 10.0)))
      .add_vulnerable("u1", (2.0, 2.0))
      .add_vulnerable("u2", (5.0, 4.0))
      .with_store("s1", (6.0, 1.0))
      .add_item("Mask", 5000, 100)
      .add_volunteer("v1", (3.0, 1.0))
      .and_add_delivery_region(((0.0, 0.0), (10.0, 10.0)))
      .and_set_worktime(wt1)
      .add_volunteer("v2", (7.0, 3.0))
      .and_add_delivery_region(((0.0, 0.0), (10.0, 10.0)))
      .and_set_worktime(wt2);
  setup
}

#[test]
fn test_1() {
  let mut setup = construct_setup(&[
    ((09, 00), (12, 00))
  ], &[
    ((10, 00), (12, 00))
  ]);
  setup.test_order("u2", "s1")
      .add_item("Mask", 1)
      .place_and_expect_volunteer("v2");
  setup.test_order("u1", "s1")
      .add_item("Mask", 1)
      .place_and_expect_volunteer("v1");
}

#[test]
fn test_2() {
  let mut setup = construct_setup(&[
    ((09, 00), (12, 00)),
    ((09, 00), (12, 00))
  ], &[
    ((24, 00), (24, 00)),
    ((09, 00), (10, 00))
  ]);
  setup.test_order("u2", "s1")
      .add_item("Mask", 1)
      .place_and_expect_volunteer("v1");
  setup.test_order("u1", "s1")
      .add_item("Mask", 1)
      .place_and_expect_volunteer("v1");
}

#[test]
fn test_3() {
  let mut setup = Setup::new();
  setup
      .fake_time(NaiveDateTime::new(NaiveDate::from_ymd(2020, 1, 1), NaiveTime::from_hms(0, 0, 0)))
      .with_council("London", ((0.0, 0.0), (10.0, 10.0)))
      .add_vulnerable("u1", (2.0, 2.0))
      .add_vulnerable("u2", (5.0, 4.0))
      .with_store("s1", (6.0, 1.0))
      .add_item("Mask", 5000, 100)
      .add_volunteer("v1", (3.0, 1.0))
      .and_add_delivery_region(((0.0, 0.0), (10.0, 10.0)))
      .and_set_worktime(&[
        ((09, 00), (10, 00))
      ])
      .and_reserve(&[
        ((09, 00), (10, 00))
      ])
      .add_volunteer("v2", (7.0, 3.0))
      .and_add_delivery_region(((0.0, 0.0), (10.0, 10.0)))
      .and_set_worktime(&[
        ((24, 00), (24, 00)),
        ((09, 00), (11, 00))
      ]);

  setup.test_order("u2", "s1")
      .add_item("Mask", 1)
      .place_and_expect_volunteer("v2");
  setup.test_order("u1", "s1")
      .add_item("Mask", 1)
      .urgent()
      .place_and_expect_volunteer("v1");
  setup.test_order("u1", "s1")
      .add_item("Mask", 1)
      .place_and_expect_volunteer("v2");
  setup.test_order("u2", "s1")
      .add_item("Mask", 1)
      .place_and_expect_fail();
  setup.test_order("u2", "s1")
      .add_item("Mask", 1)
      .urgent()
      .place_and_expect_fail();
}
