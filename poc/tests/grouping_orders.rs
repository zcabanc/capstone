use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

use test_setup::*;
use poc::AsInterval;

mod test_setup;

#[test]
fn test() {
  let mut setup = Setup::new();
  let today = NaiveDate::from_ymd(2020, 1, 1);
  setup
      .fake_time(NaiveDateTime::new(today, NaiveTime::from_hms(0, 0, 0)))
      .with_council("Manchester", ((0.0, 0.0), (2.0, 2.0)))
      .add_vulnerable("u1", (0.0, 0.0))
      .add_vulnerable("u2", (1.0, 0.0))
      .add_vulnerable("u3", (2.0, 0.0))
      .add_vulnerable("u4", (2.5, 0.0))
      .with_store("s1", (0.0, 1.0))
      .add_item("Handwash", 500, 100)
      .with_store("s2", (2.0, 1.0))
      .add_item("Handwash2", 500, 100)
      .add_volunteer("v1", (0.0, -1.0))
      .and_add_delivery_region(((0.0, 0.0), (2.0, 2.0)))
      .and_set_worktime(&[
        ((09, 00), (12, 00)),
        ((18, 00), (20, 00))
      ])
      .and_reserve(&[
        ((10, 00), (11, 00))
      ]);

  let t = setup.test_order("u1", "s1")
      .add_item("Handwash", 6)
      .place_and_expect_volunteer("v1");
  let t9am = today.and_hms(9, 0, 0);
  assert_eq!(t.time_start, t9am);
  let t = setup.test_order("u2", "s1")
      .add_item("Handwash", 4)
      .place_and_expect_volunteer("v1");
  assert_eq!(t.time_start, t9am);
  let t = setup.test_order("u3", "s1")
      .add_item("Handwash", 1)
      .place_and_expect_volunteer("v1");
  assert_eq!(t.time_start, today.and_hms(11, 0, 0));
  let t = setup.test_order("u4", "s2")
      .add_item("Handwash2", 1)
      .place_and_expect_volunteer("v1");
  assert_eq!(t.time_start, today.and_hms(18, 0, 0));
}
