use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

use test_setup::*;
use poc::AsInterval;

mod test_setup;

#[test]
fn test() {
  let mut setup = Setup::new();
  let today = NaiveDate::from_ymd(2020, 1, 1);
  setup
      .fake_time(NaiveDateTime::new(today, NaiveTime::from_hms(0, 0, 0)))
      .with_council("New York", ((0.0, 0.0), (2.0, 2.0)))
      .add_vulnerable("u1", (0.0, 0.0))
      .add_vulnerable("u2", (1.0, 0.0))
      .add_vulnerable("u3", (2.0, 0.0))
      .add_vulnerable("u4", (3.0, 0.0))
      .add_vulnerable("u5", (4.0, 0.0))
      .add_vulnerable("u6", (5.0, 0.0))
      .with_store("s1", (0.0, 1.0))
      .add_item("Toilet Paper", 2500, 100)
      .add_volunteer("v1", (0.0, -1.0))
      .and_add_delivery_region(((-1.0, 0.0), (2.0, 2.0)))
      .and_set_worktime(&[
        ((09, 00), (12, 00))
      ])
      .and_reserve(&[
        ((10, 00), (11, 00))
      ]);

  let today = setup.server.date_now();
  let t = setup.test_order("u1", "s1")
      .add_item("Toilet Paper", 1)
      .urgent()
      .place_and_expect_volunteer("v1");
  assert_eq!(t.time_start, today.and_hms(9,0,0));
  let t = setup.test_order("u2", "s1")
      .add_item("Toilet Paper", 1)
      .urgent()
      .place_and_expect_volunteer("v1");
  assert_eq!(t.time_start, today.and_hms(9,0,0));
  let t = setup.test_order("u3", "s1")
      .add_item("Toilet Paper", 1)
      .place_and_expect_volunteer("v1");
  assert_eq!(t.time_start, today.and_hms(11,0,0));
  let t = setup.test_order("u4", "s1")
      .add_item("Toilet Paper", 1)
      .urgent()
      .place_and_expect_volunteer("v1");
  assert_eq!(t.time_start, today.and_hms(11,0,0)); // Urgent can reuse non-urgent slots
  let t = setup.test_order("u5", "s1")
      .add_item("Toilet Paper", 1)
      .place_and_expect_fail();
  let t = setup.test_order("u5", "s1")
      .add_item("Toilet Paper", 1)
      .urgent()
      .place_and_expect_volunteer("v1");
  assert_eq!(t.time_start, today.and_hms(10,0,0));
  let t = setup.test_order("u6", "s1")
      .add_item("Toilet Paper", 1)
      .place_and_expect_fail(); // Non-urgent can not reuse urgent slots.
  let t = setup.test_order("u6", "s1")
      .add_item("Toilet Paper", 1)
      .urgent()
      .place_and_expect_volunteer("v1");
  assert_eq!(t.time_start, today.and_hms(10,0,0)); // Urgent can reuse urgent slots
}
